/// <reference types="cypress" />
import 'cypress-file-upload';

describe('Self Sign Automation Test', () => {
  it('Memastikan Fitur Self Sign Berjalan Dengan Baik', () => {
    cy.viewport(1280, 720);
    cy.visit('https://app.privy.id');
    cy.wait(5000);
    cy.get('[name="user[privyId]"]').type('FYM6642', { force: true });
    cy.wait(5000);
    cy.contains('CONTINUE').click({ force: true });
    cy.wait(5000);
    cy.get('[name="user[secret]"]').type('ArzaHendra2905', { force: true });
    cy.wait(5000);
    cy.contains('CONTINUE').click();
    cy.wait(5000);
    cy.get('[id="v-step-0"]').click();
    cy.wait(5000);
    cy.contains('Self Sign').click();
    cy.wait(5000);
    cy.get('[class="workflow-upload__droparea"]').attachFile(
      { filePath: 'dummy_document.pdf', allowEmpty: true },
      {
        subjectType: 'drag-n-drop',
      }
    );
    cy.wait(5000);
    cy.get('input[maxlength="200"]').clear().type('dummy_document_sample');
    cy.wait(5000);
    cy.get('button[class="btn btn-danger"]').click();
    cy.wait(5000);
    cy.get('button[id="step-document-1"]').click();
    cy.wait(5000);
    cy.get('button[class="btn btn-danger"]').click();
    cy.wait(5000);
    cy.get('button[class="btn btn-success mx-2 my-2"]').click();
    cy.wait(5000);
    cy.get('input[type="radio"]').check('qrcode', { force: true });
    cy.wait(5000);
    cy.get('button[form="form-sign"]').click();
    cy.wait(5000);
    cy.contains('Already Signed', { timeout: 300000 }).should('exist');
  });
});
